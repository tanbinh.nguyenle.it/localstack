﻿using Amazon.S3.Model;
using Amazon.S3;
using Microsoft.AspNetCore.Mvc;
using Serilog.Context;

namespace LocalStackProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class S3Controller : ControllerBase
    {
        private readonly ILogger<S3Controller> _logger;
        private IAmazonS3 _s3Client;

        public S3Controller(ILogger<S3Controller> logger, IAmazonS3 s3Client)
        {
            _logger = logger;
            _s3Client = s3Client;
        }

        [HttpGet("filecount")]
        public async Task<IActionResult> GetFileCount(string bucketName)
        {
            using (LogContext.PushProperty("S3 API", "S3 Count file"))
            {
                try
                {
                    var response = await _s3Client.ListObjectsAsync("binhdeptrai");
                    int fileCount = response.S3Objects.Count;

                    _logger.LogInformation("File Count Sucess.");

                    return Ok(new { FileCount = fileCount });
                }
                catch (AmazonS3Exception ex)
                {
                    _logger.LogError("File Count Bad Reques - " + ex.Message);

                    return BadRequest(new { Message = ex.Message });
                }
            }
        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            using (LogContext.PushProperty("S3 API", "S3 UploadFile"))
            {
                if (file == null || file.Length == 0)
                {
                    _logger.LogError("File not selected or empty.");

                    return BadRequest("File not selected or empty.");
                }

                try
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await file.CopyToAsync(memoryStream);

                        var putRequest = new PutObjectRequest
                        {
                            BucketName = "binhdeptrai",
                            Key = Guid.NewGuid().ToString(),
                            InputStream = memoryStream,
                            ContentType = file.ContentType
                        };

                        var response = await _s3Client.PutObjectAsync(putRequest);

                        if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                        {
                            _logger.LogInformation("File uploaded successfully.");

                            return Ok("File uploaded successfully.");
                        }
                        else
                        {
                            _logger.LogError("Error uploading file.");

                            return BadRequest("Error uploading file.");
                        }
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    _logger.LogError("Error uploading file. - " + ex.Message);

                    return BadRequest(new { Message = ex.Message });
                }
            }
        }
    }
}
