﻿using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService;
using Amazon.SQS.Model;
using Amazon.SQS;
using Microsoft.AspNetCore.Mvc;

namespace LocalStackProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SNSController : ControllerBase
    {
        private readonly ILogger<SNSController> _logger;
        private readonly IAmazonSimpleNotificationService _snsClient;
        private readonly IAmazonSQS _sqsClient;

        public SNSController(ILogger<SNSController> logger, IAmazonSimpleNotificationService snsClient, IAmazonSQS sqsClient)
        {
            _logger = logger;
            _snsClient = snsClient;
            _sqsClient = sqsClient;
        }

        [HttpPost("sendmessage")]
        public async Task<IActionResult> SendMessageToSQS(string message)
        {
            try
            {
                var publishRequest = new PublishRequest
                {
                    TopicArn = "arn:aws:sns:us-east-1:000000000000:AppEventTopic",
                    Message = message
                };

                var response = await _snsClient.PublishAsync(publishRequest);

                _logger.LogInformation($"Send Messsage Sucess {response.MessageId}");

                return Ok(response.MessageId);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Send Messsage Error: {ex.Message}");

                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("receivemessage")]
        public async Task<IActionResult> ReceiveMessageFromSQS()
        {
            try
            {
                var receiveMessageRequest = new ReceiveMessageRequest
                {
                    QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                    MaxNumberOfMessages = 1,
                    WaitTimeSeconds = 20
                };

                var response = await _sqsClient.ReceiveMessageAsync(receiveMessageRequest);

                if (response.Messages.Count > 0)
                {
                    var message = response.Messages[0].Body;
                    var receiptHandle = response.Messages[0].ReceiptHandle;

                    var deleteMessageRequest = new DeleteMessageRequest
                    {
                        QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                        ReceiptHandle = receiptHandle
                    };

                    await _sqsClient.DeleteMessageAsync(deleteMessageRequest);

                    _logger.LogInformation("Receive Message Success");

                    return Ok(message);
                }
                else
                {
                    _logger.LogInformation("No Messages Found In The Queue.");

                    return NotFound("No Messages Found In The Queue.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Receive Message Error: {ex.Message}");

                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
