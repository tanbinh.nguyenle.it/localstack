using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Amazon.SimpleNotificationService;
using Amazon.SQS;
using Hangfire;
using Hangfire.MemoryStorage;
using LocalStackProject.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Serilog Setup
builder.Host.UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration));

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHealthChecks();

// Hangfire setup
builder.Services.AddHangfire(x =>
{
    x.UseMemoryStorage();
});
builder.Services.AddHangfireServer();

// AWS setup
var options = new AWSOptions
{
    DefaultClientConfig =
                {
                    ServiceURL = "http://192.168.1.141:4566",
                    UseHttp = true
                }
};
builder.Services.AddAWSService<IAmazonS3>(options);
builder.Services.AddAWSService<IAmazonSQS>(options);
builder.Services.AddAWSService<IAmazonSimpleNotificationService>(options);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSerilogRequestLogging();

app.UseHangfireDashboard();

RecurringJob.AddOrUpdate<HangfireService>("process-sqs-messages", job => job.ReceiveMessageFromSQSAsync(), "*/20 * * * * *");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapHealthChecks("/health");

app.MapControllers();

app.Run();
