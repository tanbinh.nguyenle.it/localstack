﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using LocalStackProject.Controllers;
using Newtonsoft.Json.Linq;
using Serilog.Context;

namespace LocalStackProject.Services
{
    public class HangfireService
    {
        private readonly ILogger<SNSController> _logger;
        private readonly IAmazonSQS _sqsClient;
        private readonly IServiceProvider _serviceProvider;
        private IAmazonS3 _s3Client;

        public HangfireService(IAmazonSQS sqsClient, ILogger<SNSController> logger, IServiceProvider serviceProvider, IAmazonS3 s3Client)
        {
            _logger = logger;
            _sqsClient = sqsClient;
            _serviceProvider = serviceProvider;
            _s3Client = s3Client;
        }

        public async Task ReceiveMessageFromSQSAsync()
        {
            using (LogContext.PushProperty("Hangfire", "ReceiveMessage"))
            {
                _logger.LogInformation("HangFire Tricker");

                var receiveMessageRequest = new ReceiveMessageRequest
                {
                    QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                    MaxNumberOfMessages = 1,
                    WaitTimeSeconds = 20
                };

                var response = await _sqsClient.ReceiveMessageAsync(receiveMessageRequest);

                if (response.Messages.Count > 0)
                {
                    JObject responseBody = JObject.Parse(response.Messages[0].Body);

                    _logger.LogInformation(responseBody.ToString());

                    if (responseBody["Message"]?.ToString() == "Add File To S3")
                    {
                        await CreateFileAndSaveToS3Async(response.Messages[0].Body.ToString());

                        var deleteMessageRequest = new DeleteMessageRequest
                        {
                            QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                            ReceiptHandle = response.Messages[0].ReceiptHandle
                        };

                        await _sqsClient.DeleteMessageAsync(deleteMessageRequest);

                        return;
                    }
                    _logger.LogError("Message Not Enough To Add ");
                }
            }
        }

        private async Task CreateFileAndSaveToS3Async(string sqsMessage)
        {
            using (LogContext.PushProperty("Hangfire", "ReceiveMessage"))
            {

                var request = new PutObjectRequest
                {
                    BucketName = "binhdeptrai",
                    Key = Guid.NewGuid().ToString(),
                    ContentBody = sqsMessage,
                    ContentType = "text/plain"

                };

                var response = await _s3Client.PutObjectAsync(request);

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation("HangFile Add File to S3");
                    return;
                }

                _logger.LogError("Error HangFire Add File To S3");
            }
        }
    }
}
