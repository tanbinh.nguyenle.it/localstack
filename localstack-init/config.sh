#!/bin/sh
NAME="awslocal"
echo "configure $NAME!"
awslocal s3 mb s3://binhdeptrai
awslocal sqs create-queue --queue-name=AppEventQueue --attributes "ReceiveMessageWaitTimeSeconds=10,VisibilityTimeout=10"
awslocal sns create-topic --name AppEventTopic
awslocal sns subscribe --topic-arn arn:aws:sns:us-east-1:000000000000:AppEventTopic --protocol sqs --notification-endpoint arn:aws:sqs:us-east-1:000000000000:AppEventQueue
awslocal sqs create-queue --queue-name DLQ-AppEventQueue
awslocal sqs set-queue-attributes --queue-url http://localhost:4566/000000000000/AppEventQueue --attributes '{"RedrivePolicy":"{\"deadLetterTargetArn\":\"arn:aws:sqs:us-east-1:000000000000:DLQ-AppEventQueue\",\"maxReceiveCount\":\"2\"}"}'